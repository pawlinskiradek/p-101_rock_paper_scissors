# Game - Rock, Paper and Scissors

# https://gitlab.com/pawlinskiradek/p-101_rock_paper_scissors.git
# https://youtu.be/GzrcLttVehg

import random

def choose_game_type():
    print("""
                  Write what you choose:
                  rock 
                  paper
                  scissors
                  ----------------------
                  If you want to leave the game, type 'q'
                 """)
    human_choose = input().lower()
    return human_choose
 
def check_game_type(selection, choices):
    while choices not in selection and choices != "q":
        choices = input("There is no such option, please select again: ").lower()
    
def play_with(selection):
    # Choice of playing with the computer or with another player.
    game_type = input("Do you want to play against the computer (y/n)?").lower()
    
    while game_type != "y" and game_type != "n":
        game_type = input("There is no such option, please select again: ").lower()
    
    if game_type.lower() == "y":    
        player = random.choice(selection)
    else:
        player = input("Second player choice: ").lower()
        while player not in selection:
                player = input("There is no such option, please select again: ").lower()
    return player

def win_check(choices, player):
    # Comparison of your and your opponent's choices      
    if choices == player:
        print()
        print("* " * 10)
        print("it's a draw!")
        print("* " * 10)
        print()
    elif (choices == "rock" and player == "paper") or (choices == "paper" and player == "scissors") or (choices == "scissors" and player == "rock"):
        print()
        print("* " * 10)
        print(f"Your opponent chose {player}, You lost!")
        print("* " * 10)
        print()
    else:
        print()
        print("* " * 10)
        print(f"Your opponent chose {player}, You win!")
        print("* " * 10)
        print()

# options to select
options = ("rock", "paper", "scissors")

while True:
    # Your choice from options
    human_choice = choose_game_type()
    
    # If you select "q", the program will exit the "while" loop and the program will end.
    if human_choice == "q":
        break   
    
    # check if your choice is in the options (or it is "q"), if not, retry
    check_game_type(options, human_choice)
    
    # check who play with You
    who_play = play_with(options)
    
    # check who win
    win_check(human_choice, who_play)

print("* " * 10)
print("END GAME")
print("* " * 10)