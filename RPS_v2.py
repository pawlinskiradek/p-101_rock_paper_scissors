# Game - Rock, Paper and Scissors v2

from random import choice

options = ["r", "p", "s"]
wins = [("r", "s"), ("p", "r"), ("s", "p")]

user_choice = input("What You choose: (R)rock, (P)paper, (S)scissors: ").lower()

if user_choice not in options:
    print("There is no such option!")
else:
    computer_choice = choice(options)
    print(f"Computer choose {computer_choice}")
    
    if user_choice == computer_choice:
        print("it's a draw!")
    elif (user_choice, computer_choice) in wins:
        print("User wins!")
    else:
        print("Computer wins!")